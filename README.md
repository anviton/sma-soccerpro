# ``SoccerPro: simulation de match de football (SMA)`` 

## 🚀 ``Prérequis au projet`` 
Pour utiliser ``SoccerPro``, vous aurez besoin de :

- **Java** (version 11 ou supérieure)
- **IntelliJ IDEA** (recommandé)
- **Maven** (pour la gestion des dépendances et la construction du projet)

De plus, notre projet utilise des bibliothèques externes, en particulier : 

- **MASON** (mason.20.jar) : [lien bibliothèque mason.20](https://cs.gmu.edu/~eclab/projects/mason/)
- **Java3D** (j3dcore) : bibliothèque non essentielle au projet, à ajouter au projet seulement pour éviter d'avoir des avertissements au lancement de l'application. 

##  🛠️ ``Configuration`` 
Pour exécuter notre projet, voici la démarche à suivre : 
1. **Cloner le projet via GitLab**:
   Utilisez Git pour cloner le projet sur votre machine locale.

2. **Ouvrir avec IntelliJ IDEA**:
   Ouvrez le dossier du projet avec IntelliJ IDEA, reconnu comme projet Maven.

3. **Configuration de Maven**:
   Dans IntelliJ, accédez à `View > Tool Windows > Maven`. Cela ouvrira la fenêtre Maven où vous pouvez gérer les dépendances et exécuter les cycles de vie Maven.

4. **Exécution du projet**:
    Vous pouvez configurer IntelliJ pour exécuter le main (contenu dans ``Main.java``) et profiter de l'exécution automatique d'Intellij.

## 🧪 ``Tests``
Pour exécuter les phases de tests : `mvn test` ou exécuter directement le cycle de vie `test` de Maven dans Intellij.


<p align="center">
  <img src="https://i.ibb.co/8cR4mk0/Soccer-players-2024-01-06-16-15-45.gif" alt="SMA SoccerPro" width="500px" height="500px">
</p>


