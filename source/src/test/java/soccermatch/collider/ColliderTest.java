package soccermatch.collider;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import soccermatch.entities.*;

import static org.junit.jupiter.api.Assertions.*;

public class ColliderTest
{
    private Team team;
    private Field field;
    private Player player;
    private Ball ball;

    @BeforeEach
    public void setUp()
    {
        team    = new Team("A", 1);
        field   = new Field(5, 5);
        player  = new Attacker(team, "J1", 1);
        ball    = new Ball(2, 2);

        player.setX(1);
        player.setY(1);
    }

    @Test
    public void testFindAPlaceToMove() {
        int x = 2;
        int y = 2;

        // Testing when there is an empty adjacent cell
        int[] result = Collider.findAPlaceToMove(field, x, y);
        assertNotNull(result);
        assertTrue(result[0] >= 0 && result[0] < 5);
        assertTrue(result[1] >= 0 && result[1] < 5);

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                field.addPlayer(new Attacker(team, "J"+ i, 1), i, j);
            }
        }
        result = Collider.findAPlaceToMove(field, x, y);
        assertNull(result);
    }

    @Test
    public void testCalculateStep() {
        Collider.calculateAndApplyStep(ball.getX(), ball.getY(), player, field);
        assertEquals(2, player.getX());
        assertEquals(2, player.getY());

        // Move the ball away
        ball.setX(0);
        ball.setY(0);

        Collider.calculateAndApplyStep(ball.getX(), ball.getY(), player, field);
        assertEquals(1, player.getX());
        assertEquals(1, player.getY());
    }

    @Test
    public void testIsBallNear() {
        int rayon = 2;

        assertTrue(Collider.isBallNear(ball, player, rayon));

        // Increase the radius, and it should still be near
        rayon = 3;
        assertTrue(Collider.isBallNear(ball, player, rayon));

        // Move the ball further away, and it should not be near
        ball.setX(5);
        ball.setY(5);
        assertFalse(Collider.isBallNear(ball, player, rayon));

        // Test when the ball is null
        assertFalse(Collider.isBallNear(null, player, rayon));
    }
}
