package soccermatch.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest
{
    private Player player;
    private Ball ball;
    private Team team;

    @BeforeEach
    void setUp() {
        team = new Team("Team A", 1);
        player = new Attacker(team, "P1", 1);
        ball = new Ball(0, 0);
    }

    @Test
    void testTakeTheBall() {
        player.takeTheBall(ball);
        assertTrue(player.hasTheBall());
        assertTrue(ball.isPossessed);
        assertEquals(player, ball.getPlayer());
    }

    @Test
    void testIsInOwnCampWhenTeamHasBall() {
        team.takeTheBallInTeam();
        assertTrue(player.isInOwnCamp());
    }

    @Test
    void testIsInOwnCampWhenTeamDoesNotHaveBall() {
        team.looseBall();
        assertFalse(player.isInOwnCamp());
    }

}
