package soccermatch.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MatchTest {
    private Match match;
    private Team teamA;
    private Team teamB;
    private Field field;
    private Ball ball;

    @BeforeEach
    public void setUp()
    {
        teamA = new Team("Team A", 1);
        teamB = new Team("Team B", 2);
        field = new Field(40, 40);
        ball  = new Ball(0, 0);

        match = new Match(teamA, teamB, field, ball);
    }

    @Test
    public void testGetTeamA() {
        assertEquals(teamA, match.getTeamA());
    }

    @Test
    public void testGetTeamB() {
        assertEquals(teamB, match.getTeamB());
    }

    @Test
    public void testGetField() {
        assertEquals(field, match.getField());
    }

    @Test
    public void testGetBall() {
        assertEquals(ball, match.getBall());
    }

    @Test
    public void testGetTeamOpponents() {
        Attacker attacker1 = new Attacker(teamB, "J1", 1);
        Attacker attacker2 = new Attacker(teamA, "J2", 2);
        Defender defender = new Defender(teamB, "D1", 3);

        teamA.addPlayer(attacker1);
        teamA.addPlayer(attacker2);
        teamB.addPlayer(defender);

        ArrayList<Player> opponents = match.getTeamOpponents(attacker1);

        assertEquals(2, opponents.size());
    }

    @Test
    public void testGetAllPlayers() {
        Attacker attacker1 = new Attacker(teamA, "J1", 1);
        Attacker attacker2 = new Attacker(teamA, "J2", 2);
        Defender defender = new Defender(teamB, "D1", 3);

        teamA.addPlayer(attacker1);
        teamA.addPlayer(attacker2);
        teamB.addPlayer(defender);

        ArrayList<Player> allPlayers = match.getAllPlayers();

        assertEquals(3, allPlayers.size());
        assertTrue(allPlayers.contains(attacker1));
        assertTrue(allPlayers.contains(attacker2));
        assertTrue(allPlayers.contains(defender));
    }


}
