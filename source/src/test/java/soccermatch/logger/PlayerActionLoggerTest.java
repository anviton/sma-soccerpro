package soccermatch.logger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import soccermatch.entities.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;
public class PlayerActionLoggerTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testOnPlayerTookBall() {
        PlayerActionLogger logger = new PlayerActionLogger();
        Player player = new Attacker(new Team("A", 1), "J1", 1);

        logger.onPlayerTookBall(player);

        String expectedOutput = "[Team A] : J1 took the ball.\n";
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    public void testOnPlayerThrewBall() {
        PlayerActionLogger logger = new PlayerActionLogger();
        Player player1 = new Attacker(new Team("A", 1), "J1", 1);
        Player player2 = new Attacker(new Team("B", 1), "J2", 2);

        logger.onPlayerThrewBall(player1, player2);

        String expectedOutput = "[Team A] : J1 threw the ball to J2.\n";
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    public void testOnShoot() {
        PlayerActionLogger logger = new PlayerActionLogger();
        Player player = new Attacker(new Team("A", 1), "J1", 1);

        logger.onShoot(player);

        String expectedOutput = "[Team A] : J1 marked !\n";
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    public void testOnStopBall() {
        PlayerActionLogger logger = new PlayerActionLogger();
        Player player = new Goal(new Team("A", 1), "G1", 1);

        logger.onStopBall(player);

        String expectedOutput = "[Team A] : G1 stop the ball.\n";
        assertEquals(expectedOutput, outContent.toString());
    }
}
