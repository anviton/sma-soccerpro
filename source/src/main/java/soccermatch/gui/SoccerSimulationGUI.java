/**
 * The SoccerSimulationGUI class represents the graphical user interface for the soccer match simulation.
 */

package soccermatch.gui;

import sim.display.Console;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.field.continuous.Continuous2D;
import sim.portrayal.DrawInfo2D;
import sim.portrayal.SimplePortrayal2D;
import sim.portrayal.continuous.ContinuousPortrayal2D;
import sim.portrayal.simple.OvalPortrayal2D;
import sim.portrayal.simple.RectanglePortrayal2D;
import sim.util.Double2D;
import soccermatch.*;
import soccermatch.entities.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

/**
 * The SoccerSimulationGUI class represents the graphical user interface for the soccer match simulation.
 */
public class SoccerSimulationGUI extends GUIState implements Steppable {
    private Display2D        display;
    private  JFrame          displayFrame;
    private JLabel          scoreLabel         = new JLabel("SCORE: 0 - 0");
    private JLabel          possessionLabel    = new JLabel("Possession: Team A - 0 %, Team B - 0 %");
    private JPanel          actionsPanel       = new JPanel(new BorderLayout());
    private JTextArea       actionsTextArea    = new JTextArea();
    private JScrollPane     actionsScrollPane  = new JScrollPane(actionsTextArea);
    private JPanel          scorePanel         = new JPanel(new FlowLayout());
    private JPanel          possessionPanel    = new JPanel(new FlowLayout());
    private ContinuousPortrayal2D   yardPortrayal      = new ContinuousPortrayal2D();
    private RectanglePortrayal2D    goalPortrayalA     = new RectanglePortrayal2D(Color.RED, false);
    private RectanglePortrayal2D    goalPortrayalB     = new RectanglePortrayal2D(Color.BLUE, false);

    /**
     * Starts the soccer simulation GUI.
     */
    public static void run() {
        SoccerSimulationGUI vid = new SoccerSimulationGUI();
        Console c = new Console(vid);
        c.setVisible(true);
    }

    /**
     * Creates a new instance of SoccerSimulationGUI.
     */
    public SoccerSimulationGUI() {
        super(new SoccerSimulation((1023)));
        state.schedule.scheduleRepeating(this);
        scorePanel.add(scoreLabel);
        JPanel actionsContentPanel = new JPanel(new GridLayout(0, 1));
        actionsPanel.add(new JScrollPane(actionsContentPanel), BorderLayout.CENTER);

        actionsPanel.add(actionsScrollPane, BorderLayout.CENTER);
        actionsPanel.setBorder(BorderFactory.createTitledBorder("Actions"));

    }

    @Override
    public void start()
    {
        super.start();
        setupPortrayals();
    }

    /**
     * Sets up the portrayals for the soccer simulation.
     */
    public void setupPortrayals()
    {
        SoccerSimulation soccerSimulation = (SoccerSimulation) state;

        setupCommonPortrayalSettings(soccerSimulation.getYard());

        // Portrayal for Goals
        setGoalPortrayal();

        // Portrayal for Defenders
        setPlayerPortrayal(Defender.class);

        // Portrayal for Attackers
        setPlayerPortrayal(Attacker.class);

        // Portrayal for football pattern ball
        yardPortrayal.setPortrayalForClass(Ball.class, new OvalPortrayal2D(Color.BLACK, true) {
            private BufferedImage ballImage;

            {
                String path = SoccerSimulation.class.getProtectionDomain().
                        getCodeSource().getLocation().getPath();
                try {
                    // Load the pixel art image for the ball
                    File ballFile = new File(path + "../../src/img/ball.png");
                    ballImage = ImageIO.read(ballFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
                Ball ball = (Ball) object;
                int ballSize = 25;
                int x = (int) info.draw.x - ballSize / 2;
                int y = (int) info.draw.y - ballSize / 2;

                // Draw the ball as the loaded image
                graphics.drawImage(ballImage, x, y, ballSize, ballSize, null);


                // If needed, draw a border around the ball
                graphics.setColor(Color.BLACK);
                graphics.drawOval(x, y, ballSize, ballSize);
            }
        });
        // Set up portrayals for goals
        setGoalPortrayalLocations(soccerSimulation.getYard());

        display.reset();
        display.setBackdrop(new Color(14, 129, 21));
        display.repaint();
    }

    private void setupCommonPortrayalSettings(Continuous2D yard)
    {
        yardPortrayal.setField(yard);
        yardPortrayal.setBorder(true);
    }

    private void setGoalPortrayal()
    {
        yardPortrayal.setPortrayalForClass(Goal.class, new OvalPortrayal2D() {
            @Override
            public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
                Player player = (Player) object;
                graphics.setColor(Color.BLACK);
                graphics.drawString(player.getName(), (int) info.draw.x, (int) info.draw.y - 15);

                Color teamColor = player.getTeam().getId() == 1 ? Color.RED : Color.BLUE;
                paint = player.hasTheBall() && player.getTeam().getId() == 1 ? Color.ORANGE :
                        (player.hasTheBall() && player.getId() == 2 ? Color.CYAN : teamColor);

                super.draw(object, graphics, info);
            }
        });
    }

    private void setPlayerPortrayal(Class<? extends Player> playerType)
    {
        yardPortrayal.setPortrayalForClass(playerType, new SimplePortrayal2D() {
            @Override
            public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
                int playerSize = 15;
                Player player = (Player) object;

                graphics.setColor(Color.WHITE);
                graphics.fillOval((int) (info.draw.x - playerSize / 2), (int) (info.draw.y - playerSize / 2), playerSize, playerSize);
                graphics.drawString(player.getName(), (int) info.draw.x, (int) info.draw.y - 15);
                graphics.setColor(player.getTeam().getId() == 1 ? Color.RED : Color.BLUE);

                if (player.hasTheBall()) {
                    graphics.setColor(player.getTeam().getId() == 1 ? Color.ORANGE : Color.CYAN);
                    graphics.fillOval((int) (info.draw.x - playerSize / 2), (int) (info.draw.y - playerSize / 2), playerSize, playerSize);
                }

                drawArrow(player, graphics, info);
            }
        });
    }

    private void setGoalPortrayalLocations(Continuous2D yard)
    {
        Double2D goalPosA = new Double2D(yard.getWidth(), yard.getHeight() * 0.5);
        Double2D goalPosB = new Double2D(0, yard.getHeight() * 0.5);

        goalPortrayalA.scale = yard.getHeight() * 0.2; // 8
        goalPortrayalB.scale = yard.getHeight() * 0.2; // 8

        yardPortrayal.setObjectLocation(goalPortrayalA, goalPosA, this);
        yardPortrayal.setObjectLocation(goalPortrayalB, goalPosB, this);
    }


    private void drawArrow(Player player, Graphics2D graphics, DrawInfo2D info)
    {
        double angle = player.getDirection();

        int[] xPoints = {(int) info.draw.x, (int) (info.draw.x - 10 * Math.cos(angle - Math.PI / 6)),
                (int) (info.draw.x + 15 * Math.cos(angle)), (int) (info.draw.x - 10 * Math.cos(angle + Math.PI / 6))};

        int[] yPoints = {(int) info.draw.y, (int) (info.draw.y - 10 * Math.sin(angle - Math.PI / 6)),
                (int) (info.draw.y + 15 * Math.sin(angle)), (int) (info.draw.y - 10 * Math.sin(angle + Math.PI / 6))};

        graphics.fillPolygon(xPoints, yPoints, 4);
    }

    /**
     * Initializes the GUI components.
     */
    public void init(Controller c)
    {
        super.init(c);
        display = new Display2D(600, 600, this);
        displayFrame = display.createFrame();
        displayFrame.setTitle("Soccer players");

        displayFrame.setResizable(false);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(scorePanel, BorderLayout.NORTH);
        panel.add(possessionPanel, BorderLayout.SOUTH);
        panel.add(actionsPanel, BorderLayout.EAST);
        panel.add(display, BorderLayout.CENTER);

        displayFrame.setContentPane(panel);
        c.registerFrame(displayFrame);
        displayFrame.setVisible(true);

        display.attach(yardPortrayal, "Yard");
    }

    /**
     * Updates the score label with the given scores for both teams.
     *
     * @param scoreTeamA The score for Team A.
     * @param scoreTeamB The score for Team B.
     */
    public void updateScoreLabel(int scoreTeamA, int scoreTeamB)
    {
        scoreLabel.setText("SCORE : " + scoreTeamB + " - " + scoreTeamA);
    }

    /**
     * Updates the possession info for both teams.
     */
    private void updatePossessionInfo()
    {
        SoccerSimulation soccers = (SoccerSimulation) state;
        DecimalFormat df = new DecimalFormat("#");

        double possessionTeamA = soccers.getMatch().getTeamA().calculatePossession();
        double possessionTeamB = soccers.getMatch().getTeamB().calculatePossession();

        double totalPossession = possessionTeamA + possessionTeamB;

        if (totalPossession != 0) {
            possessionTeamA = (possessionTeamA / totalPossession) * 100;
            possessionTeamB = (possessionTeamB / totalPossession) * 100;
        } else {
            possessionTeamA = 0;
            possessionTeamB = 0;
        }

        possessionLabel.setText("Possession: Team A - " + df.format(possessionTeamA) + "  %, Team B - " + df.format(possessionTeamB) + " %");

        if (possessionTeamA > possessionTeamB) {
            possessionLabel.setForeground(Color.RED);
        } else if (possessionTeamA < possessionTeamB) {
            possessionLabel.setForeground(Color.BLUE);
        } else {
            possessionLabel.setForeground(Color.BLACK);
        }

        possessionPanel.add(possessionLabel);
    }

    /**
     * Quits the GUI.
     */
    public void quit()
    {
        super.quit();
        if (displayFrame != null) {
            displayFrame.dispose();
            displayFrame = null;
            display = null;
        }
    }

    @Override
    public void step(SimState simState)
    {
        SoccerSimulation state = (SoccerSimulation) super.state;
        updatePossessionInfo();
        updateScoreLabel(state.getMatch().getScoreTeamA(), state.getMatch().getScoreTeamB());
    }

    @Override
    public void finish() {
        //
    }
}
