/**
 * The PlayerActionListener interface defines methods for observing and reacting to player actions in a soccer match simulation.
 */

package soccermatch.observer;

import soccermatch.entities.Player;

/**
 * The PlayerActionListener interface defines methods for observing and reacting to player actions in a soccer match simulation.
 */
public interface PlayerActionListener {

    /**
     * Called when a player takes possession of the ball.
     *
     * @param player The player who took the ball.
     */
    void onPlayerTookBall(Player player);

    /**
     * Called when a player throws the ball to a target player.
     *
     * @param player The player who threw the ball.
     * @param target The target player who received the ball.
     */
    void onPlayerThrewBall(Player player, Player target);

    /**
     * Called when a player attempts to shoot at the goal.
     *
     * @param player The player who attempted the shot.
     */
    void onShoot(Player player);

    /**
     * Called when a player successfully stops the ball.
     *
     * @param player The player who stopped the ball.
     */
    void onStopBall(Player player);
}
