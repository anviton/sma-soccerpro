/**
 * The DisplayConsole class for displaying a soccer match in the console.
 */
package soccermatch.display;

import soccermatch.entities.Player;
import soccermatch.entities.Match;

/**
 * The DisplayConsole class displays a soccer match in the console, showing player positions and the ball's location.
 */
public class DisplayConsole extends Display {

    /**
     * This method displays player positions, marking the player with the ball in red, and the ball's location with 'o'.
     *
     * @param match The soccer match to be displayed.
     */
    public void displayMatch(Match match)
    {
        String ANSI_RED   = "\u001B[31m";
        String ANSI_RESET = "\u001B[0m";

        int[][] positions = new int[match.getField().getLength()][match.getField().getHeight()];
        for (Player j : match.getTeamB().getPlayers())
        {
            if (j.hasTheBall())
            {
                positions[j.getX()][j.getY()] = 2;
            }
            else
            {
                positions[j.getX()][j.getY()] = 1;
            }
        }
        for (Player j : match.getTeamA().getPlayers())
        {
            if (j.hasTheBall())
            {
                positions[j.getX()][j.getY()] = 2;
            }
            else
            {
                positions[j.getX()][j.getY()] = 1;
            }
        }

        for (int i = 0; i < match.getField().getLength(); i++)
        {
            for (int j = 0; j < match.getField().getHeight(); j++)
            {
                if (positions[i][j] == 1) {
                    System.out.print("x");
                }
                else if (positions[i][j] == 2)
                {
                    System.out.print(ANSI_RED + "x" + ANSI_RESET);
                }
                else if (i == match.getBall().getX() && j == match.getBall().getY())
                {
                    System.out.print("o");
                }
                else
                {
                    System.out.print(".");
                }
            }
            System.out.println();
        }
    }

}
