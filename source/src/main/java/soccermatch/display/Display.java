/**
 * The abstract Display class for displaying soccer match information.
 */
package soccermatch.display;

import soccermatch.entities.Match;

/**
 * The Display class defines a base interface for displaying match-related information.
 * Subclasses should implement the 'displayMatch' method to provide specific display functionality.
 */
public abstract class Display {

    /**
     * Subclasses should implement this method to display match-related information.
     *
     * @param match The soccer match to be displayed.
     */
    public abstract void displayMatch(Match match);
}
