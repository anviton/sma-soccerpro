/**
 * The RandomGenerator class provides a way to generate random numbers using the Mersenne Twister algorithm.
 * It is designed to be a singleton class, allowing for consistent random number generation across the application.
 */
package soccermatch.generator;

import ec.util.MersenneTwisterFast;

import java.util.Random;

public class RandomGenerator {
    private static MersenneTwisterFast instance = null;

    /**
     * Sets the Mersenne Twister Fast instance to be used for random number generation.
     * This should be called once to provide a custom instance for random number generation.
     *
     * @param mt The Mersenne Twister Fast instance to be used.
     */
    public static void setInstance(MersenneTwisterFast mt)
    {
        if (instance == null)
        {
            synchronized (RandomGenerator.class)
            {
                if (instance == null)
                {
                    instance = mt;
                }
            }
        }
    }

    /**
     * Gets the current instance of the Mersenne Twister Fast random number generator.
     *
     * @return The current Mersenne Twister Fast instance.
     */
    public static MersenneTwisterFast getInstance()
    {
        return instance;
    }
}
