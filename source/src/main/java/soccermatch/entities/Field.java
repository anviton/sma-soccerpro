/**
 * The Field class represents the playing field in the soccer simulation.
 */
package soccermatch.entities;

import sim.engine.SimState;
import sim.engine.Steppable;

/**
 * The Field class implements the Steppable interface and represents the soccer playing field.
 * It includes methods for adding players, retrieving players at specific positions, and updating player positions.
 */
public class Field implements Steppable
{
    private Player[][] zone;    ///< The 2D array representing the field's zones.
    private int length;         ///< The length (width) of the field.
    private int height;         ///< The height of the field.

    /**
     * Initializes a new instance of the Field class with the specified length and height.
     *
     * @param length The length (width) of the field.
     * @param height The height of the field.
     */
    public Field(int length, int height)
    {
        this.length = length;
        this.height = height;

        zone = new Player[length][height];
    }

    /**
     * Adds a player to the field at the specified coordinates (x, y).
     *
     * @param player The player to add.
     * @param x The x-coordinate where the player should be added.
     * @param y The y-coordinate where the player should be added.
     */
    public void addPlayer(Player player, int x, int y)
    {
        if (x >= 0 && x < length && y >= 0 && y < height)
        {
            zone[x][y] = player;
        }
    }

    /**
     * Retrieves the player at the specified coordinates (x, y) on the field.
     *
     * @param x The x-coordinate to look for the player.
     * @param y The y-coordinate to look for the player.
     * @return The player at the specified coordinates or null if no player is found.
     */
    public Player getPlayerAt(int x, int y)
    {
        if (x >= 0 && x < length && y >= 0 && y < height)
        {
            return zone[x][y];
        }
        else
        {
            return null;
        }
    }

    /**
     * Loops through the field's zones and updates player positions if they have moved.
     */
    public void updatePlayersPosition()
    {
        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Player player = zone[i][j];

                if (player != null && (player.getX() != i || player.getY() != j))
                {
                    zone[i][j] = null;
                    zone[player.getX()][player.getY()] = player;
                }
            }
        }
    }


    /**
     * Retrieves the length (width) of the field.
     *
     * @return The length of the field.
     */
    public int getLength()
    {
        return length;
    }

    /**
     * Retrieves the height of the field.
     *
     * @return The height of the field.
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * Overrides the step method to update player positions on the field.
     *
     * @param simState The current state of the simulation.
     */
    @Override
    public void step(SimState simState) {
        this.updatePlayersPosition();
    }
}
