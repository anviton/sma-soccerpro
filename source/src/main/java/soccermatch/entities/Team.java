/**
 * The Team class represents a soccer team with players and a goal.
 */

package soccermatch.entities;

import java.util.ArrayList;
import java.util.List;

public class Team
{
    private String       name;          ///< The name of the team.
    private int          id;            ///< The unique identifier of the team.
    private List<Player> players;       ///< The list of players in the team.

    private Goal         goal;          ///< The goal associated with the team.
    private boolean      hasTheBall;   ///< Indicates whether the team currently has possession of the ball.

    /**
     * Constructs a new soccer team with the given name and identifier.
     *
     * @param name The name of the team.
     * @param id   The unique identifier of the team.
     */
    public Team(String name, int id)
    {
        this.name    = name;
        this.id      = id;
        this.players = new ArrayList<>();

        this.hasTheBall = false;
    }

    /**
     * Calculates the possession percentage of the team based on the ball touches of its players.
     *
     * @return The possession percentage as a double.
     */
    public double calculatePossession()
    {
        int teamBallTouches = 0;
        for (Player player : players) {
            teamBallTouches += player.getPossession();
        }
        return (double) teamBallTouches;
    }

    /**
     * Adds a player to the team.
     *
     * @param player The player to add to the team.
     */
    public void addPlayer(Player player)
    {
        players.add(player);
    }

    /**
     * Sets the team's possession of the ball to true.
     */
    public void takeTheBallInTeam()
    {
        this.hasTheBall = true;
    }

    /**
     * Sets the team's possession of the ball to false.
     */
    public void looseBall(){ this.hasTheBall = false; }

    /**
     * Checks if the team currently has possession of the ball.
     *
     * @return True if the team has the ball, false otherwise.
     */
    public boolean hasTheBallInTeam()
    {
        return this.hasTheBall;
    }

    /**
     * Gets the list of players in the team.
     *
     * @return The list of players.
     */
    public List<Player> getPlayers()
    {
        return this.players;
    }

    /**
     * Gets the team's goal.
     *
     * @return The goal associated with the team.
     */
    public Goal getGoal(){ return this.goal; }

    /**
     * Sets the team's goal.
     *
     * @param goal The goal to set for the team.
     */
    public void setGoal(Goal goal){ this.goal = goal;}

    /**
     * Gets the name of the team.
     *
     * @return The name of the team.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the unique identifier of the team.
     *
     * @return The identifier of the team.
     */
    public int getId() {
        return id;
    }

    /**
     * Compares two teams for equality based on their identifiers.
     *
     * @param obj The object to compare with.
     * @return True if the teams have the same identifier, false otherwise.
     */
    @Override
    public boolean equals(Object obj)
    {
        if(this.getId() == ((Team)obj).getId())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

