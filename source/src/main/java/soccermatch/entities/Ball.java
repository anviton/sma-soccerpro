/**
 * The Ball class representing the soccer ball in the soccer simulation.
 */
package soccermatch.entities;

/**
 * The Ball class includes attributes and methods to manage the ball's position, possession status, and the player currently possessing it.
 */
public class Ball {
    private int      x;             ///< The x-coordinate of the ball's position.
    private int      y;             ///< The y-coordinate of the ball's position.
    private  boolean  isPossessed;   ///< A flag indicating whether the ball is currently possessed by a player.
    private Player   player;        ///< The player currently possessing the ball.


    /**
     * Initializes a new instance of the Ball class with the given initial x and y coordinates.
     *
     * @param x The initial x-coordinate of the ball's position.
     * @param y The initial y-coordinate of the ball's position.
     */
    public Ball(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Retrieves the current x-coordinate of the ball's position on the field.
     *
     * @return The x-coordinate of the ball's position.
     */
    public int getX(){
        return this.x;
    }

    /**
     * Retrieves the current y-coordinate of the ball's position on the field.
     *
     * @return The y-coordinate of the ball's position.
     */
    public int getY()
    {
        return this.y;
    }

    /**
     * Updates the x-coordinate of the ball's position to the specified value.
     *
     * @param x The new x-coordinate of the ball's position.
     */
    public void setX(int x)
    {
        this.x = x;
    }

    /**
     * Updates the y-coordinate of the ball's position to the specified value.
     *
     * @param y The new y-coordinate of the ball's position.
     */
    public void setY(int y)
    {
        this.y = y;
    }

    /**
     * Retrieves the player object representing the current possessor of the ball.
     *
     * @return The player currently possessing the ball.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Updates the player object representing the current possessor of the ball.
     *
     * @param player The player currently possessing the ball.
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isPossessed() {
        return isPossessed;
    }

    public void setPossessed(boolean possessed) {
        isPossessed = possessed;
    }

}
