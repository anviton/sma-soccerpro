/**
 * The Player class represents a soccer player in the game.
 */
package soccermatch.entities;

import sim.engine.Steppable;
import soccermatch.SoccerSimulation;
import soccermatch.collider.Collider;
import soccermatch.generator.RandomGenerator;
import soccermatch.logger.PlayerActionLogger;
import soccermatch.observer.PlayerActionListener;
import soccermatch.strategies.PlayerStrategy;

import java.util.ArrayList;
import java.util.List;

public abstract class Player implements Steppable {
    protected   int         id;             ///< The unique identifier of the player
    protected   String      name;           ///< The name of the player.

    private       boolean     hasTheBall;     ///< Indicates whether the player currently has possession of the ball.
    protected   Team        team;           ///< The team to which the player belongs.
    protected   int         x;              ///< The x-coordinate of the player's position
    protected   int         y;              ///< The y-coordinate of the player's position.
    protected   double      direction;      ///< The direction in which the player is facing.
    protected   int         possession;     ///< The number of ball possessions by the player.
    protected ArrayList<PlayerActionListener> actionListeners = new ArrayList<>(); ///< List of action listeners for player actions.
    private   PlayerStrategy                  strategy; ///< The strategy used by the player.

    /**
     * Moves the player based on the game simulation and the player's strategy.
     *
     * @param soccers The soccer simulation instance.
     */
    public abstract void move(SoccerSimulation soccers);

    /**
     * Constructs a new soccer player with the given team, name, position, and identifier.
     *
     * @param team The team to which the player belongs.
     * @param name The name of the player.
     * @param x    The initial x-coordinate of the player's position.
     * @param y    The initial y-coordinate of the player's position.
     * @param id   The unique identifier of the player.
     */
    public Player(Team team, String name, int x, int y, int id)
    {
        this.id         = id++;
        this.name       = name;
        this.team       = team;
        this.x          = x;
        this.y          = y;
        this.hasTheBall = false;

        actionListeners.add(new PlayerActionLogger());
    }

    /**
     * Checks if the player currently has possession of the ball.
     *
     * @return True if the player has the ball, false otherwise.
     */
    public boolean hasTheBall()
    {
        return this.hasTheBall;
    }


    public void setHasTheBall(boolean hasTheBall) {
        this.hasTheBall = hasTheBall;
    }

    /**
     * Takes possession of the ball and updates the team's possession status.
     *
     * @param ball The soccer ball to be taken possession of.
     */
    public void takeTheBall(Ball ball)
    {
        ball.setPlayer(this);

        this.hasTheBall = true;
        this.team.takeTheBallInTeam();

        ball.setPossessed(true);

        notifyTookBall();
    }

    /**
     * Throws the ball to a random direction within the field.
     *
     * @param ball  The soccer ball to be thrown.
     * @param field The field on which the game is played.
     */
    public void throwTheBall(Ball ball, Field field)
    {
        if (hasTheBall)
        {

            int directionX = RandomGenerator.getInstance().nextInt(SoccerSimulation.FIELD_WIDTH);
            int directionY = RandomGenerator.getInstance().nextInt(SoccerSimulation.FIELD_HEIGHT);

            Collider.calculateTrajectory(ball, directionX, directionY, field);

            hasTheBall = false;
        }
    }

    /**
     * Moves towards a safe position on the field.
     *
     * @param match The current match instance.
     */
    public void moveTowardsSafePosition(Match match)
    {
        // Example: Move towards the center of the field for a safe passing position
        int xTarget = SoccerSimulation.FIELD_WIDTH  / 2;
        int yTarget = SoccerSimulation.FIELD_HEIGHT / 2;

        this.setDirection(moveToward(xTarget, yTarget));

        Collider.calculateAndApplyStep(xTarget, yTarget, this, match.getField());
        this.setDirection(moveToward(xTarget, yTarget));
    }

    /**
     * Calculates the angle towards a specified target position.
     *
     * @param xTarget The x-coordinate of the target position.
     * @param yTarget The y-coordinate of the target position.
     *
     * @return The angle in radians towards the target position.
     */
    public double moveToward(int xTarget, int yTarget)
    {
        int dx = xTarget - x;
        int dy = yTarget - y;

        double angle = Math.atan2(dy, dx);

        angle = (angle + 2 * Math.PI) % (2 * Math.PI);

        return angle;
    }

    /**
     * Moves the player towards the ball.
     *
     * @param match The current match instance.
     */
    public void moveTowardsBall(Match match) {
        Ball ball = match.getBall();

        int xTarget = ball.getX();
        int yTarget = ball.getY();

        this.setDirection(moveToward(xTarget, yTarget));
        if (canPerform(xTarget, yTarget)){
            Collider.calculateAndApplyStep(xTarget, yTarget, this, match.getField());
        }
        else
        {
            moveTowardsSafePosition(match);
        }
    }

    /**
     * Moves the player randomly on the field.
     *
     * @param match The current match instance.
     */
    public void moveRandomly(Match match)
    {
        Field field = match.getField();
        int xTarget = RandomGenerator.getInstance().nextInt(field.getLength());
        int yTarget = RandomGenerator.getInstance().nextInt(field.getHeight());

        if (canPerform(xTarget, yTarget)) {
            Collider.calculateAndApplyStep(xTarget, yTarget, this, field);
            this.setDirection(moveToward(xTarget, yTarget));
        }
        else
        {
            moveTowardsSafePosition(match);
        }
    }

    /**
     * Calculates the distance between this player and another given player.
     *
     * @param anotherPlayer The player to calculate the distance from.
     *
     * @return The distance between the two players.
     */
    public double calculateDistanceWith(Player anotherPlayer)
    {
        int deltaX = anotherPlayer.getX() - this.getX();
        int deltaY = anotherPlayer.getY() - this.getY();

        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    /**
     * Throws the ball to the nearest player on the same team.
     *
     * @param ball The ball to be thrown.
     */
    public void throwTheBall(Ball ball) {
        if (hasTheBall)
        {
            Player target= findNearestPlayer(team.getPlayers());

            if (target != null)
            {
                hasTheBall = false;
                target.hasTheBall = true;

                ball.setX(target.getX());
                ball.setY(target.getY());

                notifyThrewBall(target);
            }
        }
    }

    /**
     * Checks if the player can perform an action towards a target position.
     *
     * @param targetX The x-coordinate of the target position.
     * @param targetY The y-coordinate of the target position.
     *
     * @return True if the player can perform the action, false otherwise.
     */
    public boolean canPerform(int targetX, int targetY)
    {
        int x = this.getX();
        int y = this.getY();

        int dx = x - targetX;
        int dy = y - targetY;

        int px = x;
        int py = y;

        return !(isInZone(x, y, Goal.GOAL_POS_X_TEAM_A - 2, Goal.GOAL_POS_Y_TEAM_A, Goal.GOAL_WIDTH, Goal.GOAL_HEIGHT))
                && !(isInZone(x, y, Goal.GOAL_POS_X_TEAM_B + 2, Goal.GOAL_POS_Y_TEAM_B, Goal.GOAL_WIDTH, Goal.GOAL_HEIGHT));
    }

    /**
     * Check if a point is within a specified rectangular zone.
     *
     * This method determines whether a given point with coordinates (pointX, pointY) is located within
     * a rectangular zone defined by its top-left corner (zoneX, zoneY), width (zoneWidth), and height (zoneHeight).
     *
     * @param pointX The X-coordinate of the point to be checked.
     * @param pointY The Y-coordinate of the point to be checked.
     * @param zoneX The X-coordinate of the top-left corner of the zone.
     * @param zoneY The Y-coordinate of the top-left corner of the zone.
     * @param zoneWidth The width of the zone.
     * @param zoneHeight The height of the zone.
     * @return True if the point is within the specified zone, otherwise false.
     */
    public static boolean isInZone(int pointX, int pointY, int zoneX, int zoneY, int zoneWidth, int zoneHeight) {
        return (pointX >= zoneX && pointX < zoneX + zoneWidth &&
                pointY >= zoneY && pointY < zoneY + zoneHeight);
    }

    /**
     * Finds the nearest player among a list of players.
     *
     * @param players The list of players to search from.
     *
     * @return The nearest player or null if no player is found.
     */
    public Player findNearestPlayer(List<Player> players) {

        Player nearestPlayer = null;

        if (!players.isEmpty())
        {
            double distanceMin = Double.MAX_VALUE;

            for (Player otherPlayer : players) {

                double distance = calculateDistanceWith(otherPlayer);

                if (distance < distanceMin && !this.equals(otherPlayer))
                {
                    distanceMin = distance;
                    nearestPlayer = otherPlayer;
                }
            }

            if (nearestPlayer != null)
            {
                this.hasTheBall = false;
                nearestPlayer.hasTheBall = true;
            }
        }
        return nearestPlayer;
    }

    /**
     * Moves the player towards the opponent's goals.
     * This method calculates a target position based on the opponent team's goals
     * and moves the player towards that position. If the player can perform the move,
     * it sets the direction and performs the movement. Otherwise, it moves towards a safe position.
     *
     * @param match The current match.
     * @param teamWithBall The team currently possessing the ball.
     */
    public void headTowardsOpponentGoals(Match match, Team teamWithBall)
    {

        int xTarget = (teamWithBall.getId() == 1) ?  5 : SoccerSimulation.FIELD_WIDTH - 5;
        int yTarget = SoccerSimulation.FIELD_HEIGHT / 2;

        if (canPerform(xTarget, yTarget))
        {
            Collider.calculateAndApplyStep(xTarget, yTarget, this, match.getField());
            this.setDirection(moveToward(xTarget, yTarget));
        }
        else
        {
            moveTowardsSafePosition(match);
        }
    }

    /**
     * Moves the player randomly within their designated zone.
     * This method selects a random target position within the player's designated zone and attempts to move towards it.
     * If the player can perform the move to the target position, it calculates the step and sets the direction accordingly.
     * If the move is not possible, the player moves towards a safe position within the field.
     *
     * @param match The current match.
     */
    public void moveRandomlyInArea(Match match)
    {
        Zone zone = this.getZone();
        int xTarget = RandomGenerator.getInstance().nextInt(zone.getWidth())  + zone.getX();
        int yTarget = RandomGenerator.getInstance().nextInt(zone.getHeight()) + zone.getY();

        if (canPerform(xTarget, yTarget))
        {
            Collider.calculateAndApplyStep(xTarget, yTarget, this, match.getField());
            this.setDirection(moveToward(xTarget, yTarget));
        }
        else
        {
            moveTowardsSafePosition(match);
        }
    }

    /**
     * Checks if the player is close to the opponent's goal zone.
     * This method determines if the player is within a close proximity to the opponent's goal zone based on
     * their current position and the field dimensions. It calculates the distances in the x and y directions
     * from the player's position to the goal zone and checks if both distances are less than or equal to 10 units.
     *
     * @param match The current match.
     * @return True if the player is close to the opponent's goal zone, false otherwise.
     */
    public boolean isCloseToOpponentGoalZone(Match match)
    {
        Team teamWithBall = match.getBall().getPlayer().team;

        int distX = Math.abs(x - ((teamWithBall.getId() == 1) ? 0 : SoccerSimulation.FIELD_WIDTH));
        int distY = Math.abs(y - SoccerSimulation.FIELD_HEIGHT / 2);

        return distX <= 10 && distY <= 10;
    }

    /**
     * Attempts to shoot at the opponent's goal.
     * This method represents an attempt by the player to shoot the ball towards the opponent's goal.
     * It involves checking the current match's goal status and determining if a goal has been scored.
     *
     * @param soccers The SoccerSimulation instance.
     */
    public void shootAtGoal(SoccerSimulation soccers)
    {
        Match match  = soccers.getMatch();
        match.checkGoal(soccers, this);
    }

    /**
     * Moves towards the opponent's goal.
     * This method guides the player to move towards the opponent's goal with the intent to score.
     * It calculates the coordinates of the opponent's goal based on the current team's ID and
     * sets the player's direction accordingly. The player then proceeds to move in that direction,
     * attempting to score a goal.
     *
     * @param match The current match.
     */
    public void moveTowardsOpponentGoal(Match match) {
        // Example: Move towards the opponent's goal to score
        int opponentGoalX;
        int opponentGoalY;

        if (getTeam().getId() == 1) {
            // If the team is Team A, move towards Team B's goal
            opponentGoalX = match.getTeamB().getGoal().getX();
            opponentGoalY = match.getTeamB().getGoal().getY();
        } else {
            // If the team is Team B, move towards Team A's goal
            opponentGoalX = match.getTeamA().getGoal().getX();
            opponentGoalY = match.getTeamA().getGoal().getY();
        }

        this.setDirection(moveToward(opponentGoalX, opponentGoalY));

        if (canPerform(opponentGoalX, opponentGoalY))
        {
            Collider.calculateAndApplyStep(opponentGoalX, opponentGoalY, this, match.getField());
        }
        else
        {
            moveTowardsSafePosition(match);
        }

    }

    /**
     * Marks the nearest opponents.
     * This method instructs the player to mark the nearest opponents from the opposing team.
     * It calculates the nearest opponent's coordinates and sets the player's direction towards
     * that opponent. The player aims to maintain close coverage of the opponent.
     *
     * @param match The current match.
     */
    public void markOpponents(Match match) {
        Player opponentPlayer = findNearestOpponent(match.getTeamOpponents(this));

        if (opponentPlayer != null)
        {
            int xTarget = opponentPlayer.getX();
            int yTarget = opponentPlayer.getY();


            if (canPerform(xTarget, yTarget))
            {
                Collider.calculateAndApplyStep(xTarget, yTarget, this, match.getField());
                this.setDirection(moveToward(xTarget, yTarget));
            }
            else
            {
                moveTowardsSafePosition(match);
            }

        }
    }

    /**
     * Move the player towards a specific zone in the opponent's goal area.
     * This method instructs the player to move towards a specific zone in the opponent's goal area
     * based on the team with possession of the ball. The player calculates the target coordinates,
     * sets the direction, and attempts to move towards the specified zone.
     *
     * @param match The current match.
     * @param teamWithBall The team currently in possession of the ball.
     */
    public  void headTowardsOpponentGoalZone(Match match, Team teamWithBall)
    {
        int xTarget = (teamWithBall.getId() == 1) ? 5 : SoccerSimulation.FIELD_WIDTH - 5;
        int yTarget = SoccerSimulation.FIELD_HEIGHT / 2;


        if (canPerform(xTarget, yTarget))
        {
            Collider.calculateAndApplyStep(xTarget, yTarget, this, match.getField());
            this.setDirection(moveToward(xTarget, yTarget));
        }
        else
        {
            moveTowardsSafePosition(match);
        }
    }

    /**
     * Check if the player is in its own team's camp.
     * This method checks whether the player is currently located within its own team's camp.
     *
     * @return True if the player is in its own camp, otherwise false.
     */
    public boolean isInOwnCamp()
    {
        return this.getTeam().hasTheBallInTeam();
    }

    /**
     * Find the nearest opponent player.
     * This method calculates the nearest opponent player among a list of opponents.
     *
     * @param opponents A list of opponent players to search from.
     * @return The nearest opponent player or null if no opponents are found.
     */
    public Player findNearestOpponent(ArrayList<Player> opponents) {
        Player nearestOpponent = null;

        if (!opponents.isEmpty()) {
            double distanceMin = Double.MAX_VALUE;

            for (Player opponent : opponents) {
                double distance = calculateDistanceWith(opponent);

                if (distance < distanceMin) {
                    distanceMin = distance;
                    nearestOpponent = opponent;
                }
            }
        }
        return nearestOpponent;
    }

    /**
     * Add an action listener to the player.
     * This method allows you to add an action listener to the player, which can be used to
     * receive notifications about player actions.
     *
     * @param listener The PlayerActionListener to add.
     */
    public void addActionListener(PlayerActionListener listener) {
        actionListeners.add(listener);
    }

    /**
     * Remove an action listener from the player.
     * This method allows you to remove an action listener from the player.
     *
     * @param listener The PlayerActionListener to remove.
     */
    public void removeActionListener(PlayerActionListener listener) {
        actionListeners.remove(listener);
    }

    /**
     * Notify that the player took possession of the ball.
     * This method notifies all registered action listeners that the player took possession of the ball.
     */
    protected void notifyTookBall() {
        for (PlayerActionListener listener : actionListeners) {
            listener.onPlayerTookBall(this);
        }
    }

    /**
     * Notify that the player threw the ball to a target player.
     * This method notifies all registered action listeners that the player threw the ball to a target player.
     *
     * @param target The target player who received the ball.
     */
    protected void notifyThrewBall(Player target) {
        for (PlayerActionListener listener : actionListeners) {
            listener.onPlayerThrewBall(this, target);
        }
    }

    /**
     * Notify that the player performed a shoot action.
     * This method notifies all registered action listeners that the player performed a shoot action.
     *
     */
    protected void notifyShoot()
    {
        for (PlayerActionListener listener : actionListeners)
        {
            listener.onShoot(this);
        }
    }

    /**
     * Notify that the player performed a stop ball action.
     * This method notifies all registered action listeners that the player performed a stop ball action.
     *
     */
    protected void notifyStopBall()
    {
        for (PlayerActionListener listener : actionListeners)
        {
            listener.onStopBall(this);
        }
    }

    /**
     * Get the status code for the player's class.
     * This method returns a status code indicating the player's class type, which can be "J" for Attacker,
     * "D" for Defender, or "G" for Goalkeeper.
     *
     * @param c The class representing the player's role.
     * @return A status code indicating the player's class type.
     */
    public String getStatus(Class c) {
        if (Attacker.class.isAssignableFrom(c)) {
            return "J";
        } else if (Defender.class.isAssignableFrom(c)) {
            return "D";
        } else if (Goal.class.isAssignableFrom(c))
        {
            return "G";
        }
        return "";
    }

    /**
     * Set the player's strategy.
     *
     * This method allows you to set the player's strategy, which determines their behavior and actions.
     *
     * @param strategy The PlayerStrategy to set for the player.
     */
    public void setStrategy(PlayerStrategy strategy)
    {
        this.strategy = strategy;
    }

    /**
     * Execute the player's assigned strategy.
     * This method executes the player's assigned strategy, if one is set.
     *
     * @param simulation The SoccerSimulation instance.
     */
    public void executeStrategy(SoccerSimulation simulation)
    {
        if (null != strategy)
        {
            strategy.execute(this, simulation);
        }
    }

    /**
     * Get the player's name.
     *
     * @return The name of the player.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the X-coordinate of the player's current position.
     *
     * @return The X-coordinate of the player's position.
     */
    public int getX()
    {
        return x;
    }

    /**
     * Set the X-coordinate of the player's current position.
     *
     * @param newX The new X-coordinate for the player's position.
     */
    public void setX(int newX)
    {
        this.x = newX;
    }

    /**
     * Get the Y-coordinate of the player's current position.
     *
     * @return The Y-coordinate of the player's position.
     */
    public int getY()
    {
        return y;
    }

    /**
     * Set the Y-coordinate of the player's current position.
     *
     * @param newY The new Y-coordinate for the player's position.
     */
    public void setY(int newY)
    {
        this.y = newY;
    }

    /**
     * Get the team to which the player belongs.
     *
     * @return The team to which the player belongs.
     */
    public Team getTeam()
    {
        return this.team;
    }

    /**
     * Get the unique identifier (ID) of the player.
     *
     * @return The ID of the player.
     */
    public int getId()
    {
        return this.id;
    }

    /**
     * Get the direction in which the player is facing.
     *
     * @return The direction in radians.
     */
    public double getDirection()
    {
        return this.direction;
    }

    /**
     * Set the direction in which the player is facing.
     *
     * @param newDirection The new direction in radians.
     */
    public void setDirection(double newDirection)
    {
        this.direction = newDirection;
    }

    /**
     * Get the zone associated with the player.
     *
     * This method should be implemented by subclasses to return the specific zone for the player.
     *
     * @return The zone associated with the player.
     */
    public abstract Zone getZone();

    /**
     * Get the player's possession count.
     *
     * @return The number of times the player has possessed the ball.
     */
    public int getPossession(){ return this.possession; }

    /**
     * Increment the player's possession count.
     *
     * This method increments the player's possession count, indicating a successful possession of the ball.
     */
    public void incrementPossession(){ possession++; }

}

