/**
 * The Goal class represents a soccer goal in the simulation.
 */
package soccermatch.entities;

import sim.engine.SimState;
import sim.util.Double2D;
import soccermatch.*;
import soccermatch.collider.Collider;
import soccermatch.generator.RandomGenerator;
import soccermatch.strategies.goal.GoalStrategy;

/**
 * Represents a soccer goal in the simulation.
 * The Goal class extends the Player class and represents a soccer goal. It includes
 * properties and methods specific to the goal, such as its position and goal zone.
 */
public class Goal extends Player
{
    public static int GOAL_POS_X_TEAM_A =  SoccerSimulation.FIELD_WIDTH - 8;    ///< The x-coordinate of the goal for Team A.
    public static int GOAL_POS_Y_TEAM_A = (SoccerSimulation.FIELD_WIDTH / 2);   ///< The y-coordinate of the goal for Team A.

    public static int GOAL_POS_X_TEAM_B = 0;                                    ///< The x-coordinate of the goal for Team B.
    public static int GOAL_POS_Y_TEAM_B = (SoccerSimulation.FIELD_WIDTH / 2);   ///< The y-coordinate of the goal for Team B.

    public static int GOAL_WIDTH  = 8; ///< The width of the goal.
    public static int GOAL_HEIGHT = 3; ///< The height of the goal.

    private Zone goalZone; ///< The goal's zone, specifying its position and dimensions.

    /**
     * Initializes a new instance of the Goal class with the specified parameters.
     *
     * @param team The team to which the goal belongs.
     * @param name The name of the goal.
     * @param id The unique identifier of the goal.
     */
    public Goal(Team team, String name, int id)
    {
        super(team, name,0, 0, id);

        if (team.getId() == 1)
        {
            this.goalZone = new Zone(SoccerSimulation.FIELD_WIDTH - 8, SoccerSimulation.FIELD_WIDTH / 2, 8, 3);
        }
        else
        {
            this.goalZone = new Zone(8, SoccerSimulation.FIELD_WIDTH / 2, 8, 3);
        }
        this.setStrategy(new GoalStrategy());
        initializePosition();
    }

    /**
     * Sets the initial position of the goal based on the team it belongs to.
     */
    private void initializePosition()
    {
        x = (this.getTeam().getId() == 1 ? goalZone.getX() + goalZone.getWidth() / 2: goalZone.getWidth() / 2);
        y = goalZone.getY();
    }

    /**
     * Resets the position of the goal to its initial position.
     */
    public void resetGoalPosition()
    {
        initializePosition();
    }

    /**
     * Overrides the move method to execute the goal's strategy.
     *
     * @param goals The soccer simulation.
     */
    @Override
    public void move(SoccerSimulation goals) {
        executeStrategy(goals);
    }

    /**
     * Retrieves the zone associated with the goal, specifying its position and dimensions.
     *
     * @return The goal's zone.
     */
    @Override
    public Zone getZone(){ return this.goalZone; }

    /**
     * Overrides the step method to update the goal's position in the simulation.
     *
     * @param simState The current state of the simulation.
     */
    @Override
    public void step(SimState simState)
    {
        SoccerSimulation soccers = (SoccerSimulation) simState;
        move(soccers);

        soccers.getYard().setObjectLocation(this, new Double2D(x, y));

    }
}
