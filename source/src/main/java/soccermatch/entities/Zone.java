/**
 * The Zone class represents a rectangular area with defined dimensions and position.
 */
package soccermatch.entities;

public class Zone {
    private int x;          ///< The x-coordinate of the top-left corner of the zone.
    private int y;          ///< The y-coordinate of the top-left corner of the zone.
    private int width;      ///< The width of the zone.
    private int height;     ///< The height of the zone.


    /**
     * Constructs a new Zone with the specified position and dimensions.
     *
     * @param x      The x-coordinate of the top-left corner of the zone.
     * @param y      The y-coordinate of the top-left corner of the zone.
     * @param width  The width of the zone.
     * @param height The height of the zone.
     */
    public Zone(int x, int y, int width, int height)
    {
        this.x      = x;
        this.y      = y;
        this.width  = width;
        this.height = height;
    }

    /**
     * Checks if a point with the given coordinates is contained within this zone.
     *
     * @param x The x-coordinate of the point.
     * @param y The y-coordinate of the point.
     * @return True if the point is inside the zone, false otherwise.
     */
    public boolean contains(int x, int y)
    {
        return x >= this.x && x <= this.x + this.width && y >= this.y && y <= this.y + this.height;
    }

    /**
     * Gets the x-coordinate of the top-left corner of the zone.
     *
     * @return The x-coordinate.
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the x-coordinate of the top-left corner of the zone.
     *
     * @param x The new x-coordinate.
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Gets the y-coordinate of the top-left corner of the zone.
     *
     * @return The y-coordinate.
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the y-coordinate of the top-left corner of the zone.
     *
     * @param y The new x-coordinate.
     */
    public void setY(int y){ this.y = y; }

    /**
     * Gets the width of the zone.
     *
     * @return The width.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the height of the zone.
     *
     * @return The height.
     */
    public int getHeight() {
        return height;
    }

}
