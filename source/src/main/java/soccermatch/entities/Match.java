/**
 * The Match class represents a soccer match simulation.
 */
package soccermatch.entities;

import soccermatch.SoccerSimulation;
import soccermatch.generator.RandomGenerator;

import java.util.ArrayList;

/**
 * Represents a soccer match simulation.
 * This class provides the structure and functionality for a soccer match simulation.
 */
public class Match {
    private Team    teamA;         ///< The first team participating in the match.
    private Team    teamB;         ///< The second team participating in the match.
    private Field   field;         ///< The playing field for the match.
    private Ball    ball;          ///< The soccer ball used in the match.
    private int     scoreTeamA;    ///< The score of the first team.
    private int     scoreTeamB;   ///< The score of the second team.


    /**
     * Initializes a new instance of the Match class with the specified parameters.
     *
     * @param teamA The first team participating in the match.
     * @param teamB The second team participating in the match.
     * @param field The playing field for the match.
     * @param ball The soccer ball used in the match.
     */
    public Match(Team teamA, Team teamB, Field field, Ball ball) {
        this.teamA = teamA;
        this.teamB = teamB;
        this.field = field;
        this.ball = ball;
    }

    /**
     * Gets the first team participating in the match.
     *
     * @return The first team.
     */
    public Team getTeamA() {
        return teamA;
    }

    /**
     * Gets the second team participating in the match.
     *
     * @return The second team.
     */
    public Team getTeamB() {
        return teamB;
    }

    /**
     * Gets the playing field for the match.
     *
     * @return The playing field.
     */
    public Field getField() {
        return field;
    }

    /**
     * Gets the soccer ball used in the match.
     *
     * @return The soccer ball.
     */
    public Ball getBall() {
        return ball;
    }

    /**
     * Checks if a goal is scored during the match and updates the score accordingly.
     * This method simulates the scoring of a goal during the match and adjusts the score
     * and game state accordingly.
     *
     * @param soccers The soccer simulation.
     * @param attacker The player attempting to score the goal.
     */
    public void checkGoal(SoccerSimulation soccers, Player attacker) {
        Goal goal;

        if (attacker.getTeam().equals(teamA)) {
            goal = teamB.getGoal();
        } else {
            goal = teamA.getGoal();
        }

        boolean stopByGuard = (RandomGenerator.getInstance().nextDouble() < 0.9) ? true : false;

        if (stopByGuard) {
            ball.setPossessed(false);
            attacker.setHasTheBall(false);
            attacker.team.looseBall();

            goal.throwTheBall(ball, field);
            goal.notifyStopBall();

        } else {
            ball.setPossessed(false);
            attacker.setHasTheBall(false);
            attacker.getTeam().looseBall();

            if (attacker.getTeam().equals(teamA)) {
                scoreTeamA++;
            } else {
                scoreTeamB++;
            }

            attacker.notifyShoot();
            soccers.resetPosition();
        }
    }

    /**
     * Gets the list of opponents for a given player.
     * This method retrieves the list of opponents from the opposing team for a given player.
     *
     * @param player The player for whom opponents are sought.
     * @return The list of opponents for the player.
     */
    public ArrayList<Player> getTeamOpponents(Player player) {
        Team opponentsTeam = (player.getTeam().getId() == 1 ? this.getTeamB() : this.getTeamA());

        ArrayList<Player> opponents = new ArrayList<>();

        for (Player oppPlayer : opponentsTeam.getPlayers()) {
            if (oppPlayer instanceof Attacker) {
                opponents.add(oppPlayer);
            }
        }
        return opponents;
    }

    /**
     * Gets a list of all players participating in the match.
     * This method retrieves a list of all players from both teams participating in the match.
     *
     * @return The list of all players in the match.
     */
    public ArrayList<Player> getAllPlayers() {
        ArrayList<Player> allPlayers = new ArrayList<>();

        // Add players from teamA
        for (Player player : teamA.getPlayers()) {
            allPlayers.add(player);
        }

        // Add players from teamB
        for (Player player : teamB.getPlayers()) {
            allPlayers.add(player);
        }

        return allPlayers;
    }

    /**
     * Gets the score of the first team.
     *
     * @return The score of the first team.
     */
    public int getScoreTeamA() {
        return scoreTeamA;
    }

    /**
     * Sets the score of the first team.
     *
     * @param scoreTeamA The new score for the first team.
     */
    public void setScoreTeamA(int scoreTeamA) {
        this.scoreTeamA = scoreTeamA;
    }

    /**
     * Gets the score of the second team.
     *
     * @return The score of the second team.
     */
    public int getScoreTeamB() {
        return scoreTeamB;
    }

    /**
     * Sets the score of the second team.
     *
     * @param scoreTeamB The new score for the second team.
     */
    public void setScoreTeamB(int scoreTeamB) {
        this.scoreTeamB = scoreTeamB;
    }

}
