/**
 * The Attacker class representing an attacker player in the soccer simulation.
 */

package soccermatch.entities;

import sim.engine.SimState;
import sim.util.Double2D;
import soccermatch.*;
import soccermatch.generator.RandomGenerator;
import soccermatch.strategies.attacker.AttackerStrategy;

/**
 * The Attacker class includes attributes and methods specific to attacker players, such as their movement strategies and zones.
 */
public class Attacker extends Player
{

    private Zone attackerZone; ///< The zone in which an attacker player operates.

    /**
     * Initializes a new Attacker object with the provided team, name, initial x and y coordinates, and unique identifier (ID).
     *
     * @param team The team to which the attacker belongs.
     * @param name The name of the attacker.
     * @param id   The unique identifier (ID) of the attacker.
     */
    public Attacker(Team team, String name, int id)
    {
        super(team, name, 0, 0, id);
        if (team.getId() == 1)
        {
            this.attackerZone = new Zone(0, 0, SoccerSimulation.FIELD_WIDTH / 2, SoccerSimulation.FIELD_HEIGHT);
        }
        else
        {
            this.attackerZone = new Zone(SoccerSimulation.FIELD_WIDTH / 2, 0, SoccerSimulation.FIELD_WIDTH / 2, SoccerSimulation.FIELD_HEIGHT);
        }

        this.setStrategy(new AttackerStrategy());
    }

    /**
     * Executes the movement strategy specific to attacker players.
     *
     * @param attackers The SoccerSimulation instance.
     */
    @Override
    public void move(SoccerSimulation attackers)
    {
        executeStrategy(attackers);
    }

    /**
     * Retrieves the zone specific to attacker players, which defines their operational area on the field.
     *
     * @return The attacker's zone.
     */
    @Override
    public Zone getZone() {
        return attackerZone;
    }

    /**
     * Handles the step function for attacker players, including movement, ball possession, and passing.
     *
     * @param simState The current simulation state.
     */
    @Override
    public void step(SimState simState)
    {
        SoccerSimulation soccers = (SoccerSimulation) simState;
        Ball ball                = soccers.getMatch().getBall();

        move(soccers);

        if (this.hasTheBall()){
            boolean tryAPass = RandomGenerator.getInstance().nextDouble() < 0.3;
            if (tryAPass)
            {
                throwTheBall(ball);
            }
        }

        soccers.getYard().setObjectLocation(this, new Double2D(x, y));
        soccers.getYard().setObjectLocation(soccers.getMatch().getBall(),
                new Double2D(ball.getX(), ball.getY()));
    }
}
