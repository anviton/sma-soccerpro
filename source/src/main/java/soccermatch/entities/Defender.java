/**
 * The Defender class representing a defender player in the soccer simulation.
 */
package soccermatch.entities;

import sim.engine.SimState;
import sim.util.Double2D;
import soccermatch.*;
import soccermatch.generator.RandomGenerator;
import soccermatch.strategies.defender.DefenderStrategy;

/**
 * The Defender class extends the Player class and implements the behavior of a defender player.
 * It includes methods for moving, getting the defender's zone, and handling player steps.
 */
public class Defender extends Player {

    private Zone defenderZone; ///< The zone where the defender operates.

    /**
     * Initializes a new instance of the Defender class with the specified team, name, and identifier.
     *
     * @param team The team to which the defender belongs.
     * @param name The name of the defender.
     * @param id The unique identifier of the defender.
     */
    public Defender(Team team, String name, int id) {
        super(team, name, 0, 0, id);

        if (team.getId() == 1) {
            this.defenderZone = new Zone(0, 0, SoccerSimulation.FIELD_WIDTH / 2, SoccerSimulation.FIELD_HEIGHT);
        } else {
            this.defenderZone = new Zone(2 * SoccerSimulation.FIELD_WIDTH / 2, 0, SoccerSimulation.FIELD_WIDTH / 3, SoccerSimulation.FIELD_HEIGHT);
        }

        this.setStrategy(new DefenderStrategy());
    }

    /**
     * Moves the defender according to its strategy within the soccer simulation.
     *
     * @param defenders The current state of the soccer simulation.
     */
    @Override
    public void move(SoccerSimulation defenders)
    {
        executeStrategy(defenders);
    }

    /**
     * Retrieves the defender's operational zone on the soccer field.
     *
     * @return The defender's zone.
     */
    @Override
    public Zone getZone() {
        return defenderZone;
    }

    /**
     * Overrides the step method to handle defender actions during the simulation step.
     *
     * @param simState The current state of the simulation.
     */
    @Override
    public void step(SimState simState) {
        SoccerSimulation soccers = (SoccerSimulation) simState;
        Ball ball = soccers.getMatch().getBall();

        move(soccers);

        if (hasTheBall()) {
            boolean tryAPass = RandomGenerator.getInstance().nextDouble() < 0.2;
            if (tryAPass) {
                throwTheBall(ball);
            }
        }

        soccers.getYard().setObjectLocation(this, new Double2D(x, y));
        soccers.getYard().setObjectLocation(soccers.getMatch().getBall(),
                new Double2D(ball.getX(), ball.getY()));
    }
}
