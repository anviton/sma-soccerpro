/**
 * The AttackerStrategy class implements a strategy for controlling the behavior of an attacker player in a soccer match simulation.
 */

package soccermatch.strategies.attacker;

import soccermatch.SoccerSimulation;
import soccermatch.collider.Collider;
import soccermatch.entities.Ball;
import soccermatch.entities.Match;
import soccermatch.entities.Player;
import soccermatch.entities.Team;
import soccermatch.generator.RandomGenerator;
import soccermatch.strategies.PlayerStrategy;

import java.util.Objects;

/**
 * The AttackerStrategy class implements a strategy for controlling the behavior of an attacker player in a soccer match simulation.
 */
public class AttackerStrategy implements PlayerStrategy {

    /**
     * Executes the attacker player strategy for the given player within the soccer simulation.
     *
     * @param player     The attacker player for which the strategy is executed.
     * @param simulation The soccer simulation in which the player is participating.
     */
    @Override
    public void execute(Player player, SoccerSimulation simulation)
    {
        Match match   = simulation.getMatch();
        Ball ball     = match.getBall();

        if (ball.isPossessed())
        {
            Team teamWithBall = ball.getPlayer().getTeam();

            if (player.hasTheBall())
            {
                double random = RandomGenerator.getInstance().nextDouble();

                if (player.isCloseToOpponentGoalZone(match))
                {
                    if (random < 0.4)
                    {
                        player.shootAtGoal(simulation);
                    }
                    else
                    {
                        player.throwTheBall(ball);
                    }

                    player.setHasTheBall(false);
                }
                else
                {
                    player.headTowardsOpponentGoals(match, teamWithBall);
                }
            }
            else if (Objects.equals(player.getTeam(), teamWithBall))
            {
                double random = RandomGenerator.getInstance().nextDouble();
                if (random < 0.9) {
                    player.headTowardsOpponentGoals(match, teamWithBall);
                }
                else
                {
                    player.moveRandomlyInArea(match);
                }
            }
            else
            {
                player.moveTowardsBall(match);
            }
        }
        else
        {
            player.moveTowardsBall(match);
            if (Collider.isBallNear(ball, player,1) && !player.hasTheBall() && !ball.isPossessed()) {
                player.takeTheBall(ball);
            }

            if (Collider.isBallNear(ball, player, 1) && !player.hasTheBall() && ball.isPossessed() && !player.getTeam().hasTheBallInTeam())
            {
                ball.getPlayer().getTeam().looseBall();
                ball.getPlayer().setHasTheBall(false);
                player.takeTheBall(ball);
            }
        }

        if (player.hasTheBall())
        {
            ball.setX(player.getX());
            ball.setY(player.getY());
            player.incrementPossession();
        }
    }
}
