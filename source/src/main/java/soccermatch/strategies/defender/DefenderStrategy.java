/**
 * The DefenderStrategy class implements a strategy for controlling the behavior of a defender player in a soccer match simulation.
 */

package soccermatch.strategies.defender;

import soccermatch.SoccerSimulation;
import soccermatch.collider.Collider;
import soccermatch.entities.Ball;
import soccermatch.entities.Match;
import soccermatch.entities.Player;
import soccermatch.entities.Team;
import soccermatch.generator.RandomGenerator;
import soccermatch.strategies.PlayerStrategy;

import java.util.Objects;

/**
 * The DefenderStrategy class implements a strategy for controlling the behavior of a defender player in a soccer match simulation.
 */
public class DefenderStrategy implements PlayerStrategy {

    /**
     * Executes the defender player strategy for the given player within the soccer simulation.
     *
     * @param player     The defender player for which the strategy is executed.
     * @param simulation The soccer simulation in which the player is participating.
     */
    @Override
    public void execute(Player player, SoccerSimulation simulation)
    {
        Match match = simulation.getMatch();
        Ball ball = match.getBall();

        double random = RandomGenerator.getInstance().nextDouble();

        if (ball.isPossessed()) {
            Team teamPossessingBall = ball.getPlayer().getTeam();

            if (player.hasTheBall()) {
                if (player.isInOwnCamp()) {
                    player.moveTowardsOpponentGoal(match);
                } else {
                    player.moveTowardsSafePosition(match);
                }
            } else if (Objects.equals(player.getTeam(), teamPossessingBall)) {
                player.moveTowardsSafePosition(match);
            }
            else if (random < 0.5)
            {
                player.moveRandomly(match);
            }
            else
            {
                player.moveTowardsBall(match);
            }
        }
        else
        {
            player.moveTowardsBall(match);
            if (Collider.isBallNear(ball, player, 1) && !player.hasTheBall() && !ball.isPossessed()) {
                player.takeTheBall(ball);
            }
        }
        if (player.hasTheBall()) {
            // If the defender has the ball, update its position
            ball.setX(player.getX());
            ball.setY(player.getY());
            player.incrementPossession();
        }
    }
}
