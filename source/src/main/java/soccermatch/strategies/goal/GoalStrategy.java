/**
 * The GoalStrategy class implements a strategy for controlling the behavior of a goal player in a soccer match simulation.
 */

package soccermatch.strategies.goal;

import soccermatch.SoccerSimulation;
import soccermatch.collider.Collider;
import soccermatch.entities.Match;
import soccermatch.entities.Player;
import soccermatch.generator.RandomGenerator;
import soccermatch.strategies.PlayerStrategy;

/**
 * The GoalStrategy class implements a strategy for controlling the behavior of a goal player in a soccer match simulation.
 */
public class GoalStrategy implements PlayerStrategy {

    /**
     * Executes the goal player strategy for the given player within the soccer simulation.
     *
     * @param player     The goal player for which the strategy is executed.
     * @param simulation The soccer simulation in which the player is participating.
     */
    @Override
    public void execute(Player player, SoccerSimulation simulation) {
        Match match = simulation.getMatch();

        if (Collider.isBallNear(match.getBall(), player, 2)) {
            int dx = match.getBall().getX() - player.getX();
            int dy = match.getBall().getY() - player.getY();

            boolean ballInGoalDirection = (player.getTeam().getId() == 1 && dx > 0) || (player.getTeam().getId() == 2 && dx < 0);

            if (player.getZone().contains(player.getX() + dx, player.getY() + dy) && ballInGoalDirection)
            {
                Collider.calculateAndApplyStep(player.getX() + dx, player.getY() + dy, player, match.getField());
            }
            else
            {
                int middleX = player.getZone().getX() + player.getZone().getWidth() / 2;
                int middleY = player.getZone().getY() + player.getZone().getHeight() / 2;

                if (player.canPerform(middleX, middleY))
                {
                    Collider.calculateAndApplyStep(middleX, middleY, player, match.getField());
                }
            }
        }
        else {
            player.setY(RandomGenerator.getInstance().nextInt(player.getZone().getHeight()) + player.getZone().getY());
        }
    }
}
