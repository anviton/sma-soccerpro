/**
 * The PlayerStrategy interface defines a strategy for controlling the behavior of a soccer player.
 */
package soccermatch.strategies;

import soccermatch.SoccerSimulation;
import soccermatch.entities.Player;

public interface PlayerStrategy {

    /**
     * Executes the player strategy for the given player within the soccer simulation.
     *
     * @param player     The player for which the strategy is executed.
     * @param simulation The soccer simulation in which the player is participating.
     */
    void execute(Player player, SoccerSimulation simulation);
}
