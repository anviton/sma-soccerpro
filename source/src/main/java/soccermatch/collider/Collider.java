/**
 * The Collider class responsible for collision-related calculations in the soccer simulation.
 */
package soccermatch.collider;

import soccermatch.entities.Ball;
import soccermatch.entities.Player;
import soccermatch.entities.Field;

/**
 * The Collider class includes methods to calculate player movement steps, detect proximity to the ball, and calculate the ball's trajectory.
 */
public class Collider
{
    /**
     * Given the field and player's current position, this method searches for an available neighboring position for the player.
     *
     * @param field The soccer field.
     * @param x     The current x-coordinate of the player.
     * @param y     The current y-coordinate of the player.
     * @return An array containing the new x and y coordinates for the player's movement, or null if no available position is found.
     */
    public static int [] findAPlaceToMove(Field field, int x, int y)
    {
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                int newX = x + i;
                int newY = y + j;

                if (newX >= 0 && newX < field.getLength() && newY >= 0 && newY < field.getHeight())
                {
                    if (field.getPlayerAt(newX, newY) == null)
                    {
                        return new int[]{newX, newY};
                    }
                }
            }
        }
        return null;
    }

    /**
     * @brief Calculates and apply the next step for a player's movement towards a specified destination.
     *
     * Given the player's current position, destination coordinates (bx, by), and the field, this method calculates the next step for the player's movement.
     *
     * @param bx     The x-coordinate of the destination.
     * @param by     The y-coordinate of the destination.
     * @param player The player whose movement is being calculated.
     * @param field  The soccer field.
     */
    public static void calculateAndApplyStep(int bx, int by, Player player, Field field)
    {
        int x = player.getX();
        int y = player.getY();

        int dx = x - bx;
        int dy = y - by;

        int px = x;
        int py = y;

        int[] placeToMove;

        if (dx < 0 && dy < 0)
        {
            if (x + 1 < field.getLength() && y + 1 < field.getHeight() &&
                    field.getPlayerAt(x + 1, y + 1) == null)
            {
                x++;
                y++;
            }
        }
        else if (dx > 0 && dy < 0)
        {
            if (x - 1 >= 0 && y + 1 < field.getHeight() &&
                    field.getPlayerAt(x - 1, y + 1) == null)
            {
                x--;
                y++;
            }
        }
        else if (dx < 0 && dy > 0)
        {
            if (x + 1 < field.getLength() && y - 1 >= 0 &&
                    field.getPlayerAt(x + 1, y - 1) == null)
            {
                x++;
                y--;
            }
        }
        else if (dx > 0 && dy > 0)
        {
            if (x - 1 >= 0 && y - 1 >= 0 &&
                    field.getPlayerAt(x - 1, y - 1) == null)
            {
                x--;
                y--;
            }
        } else if (dx < 0)
        {
            if (x + 1 < field.getLength() &&
                    field.getPlayerAt(x + 1, y) == null)
            {
                x++;
            }
        } else if (dx > 0)
        {
            if (x - 1 >= 0 && field.getPlayerAt(x - 1, y) == null)
            {
                x--;
            }
        } else if (dy < 0)
        {
            if (y + 1 < field.getHeight() && field.getPlayerAt(x, y + 1) == null) {
                y++;
            }
        } else if (dy > 0)
        {
            if (y - 1 >= 0 && field.getPlayerAt(x, y - 1) == null)
            {
                y--;
            }
        }

        if (px == x && py == y)
        {
            placeToMove = findAPlaceToMove(field, x, y);
            if (null != placeToMove)
            {
                x = placeToMove[0];
                y = placeToMove[1];
            }
        }

        player.setX(x);
        player.setY(y);

        field.updatePlayersPosition();
    }

    /**
     * Calculates the distance between a player and the ball using their respective positions.
     *
     * @param player The player.
     * @param ball   The ball.
     * @return The Euclidean distance between the player and the ball.
     */
    private static double getDistance(Player player, Ball ball)
    {
        return Math.sqrt(Math.pow(ball.getX() - player.getX(), 2) + Math.pow(ball.getY() - player.getY(), 2));
    }

    /**
     * Determines whether the ball is within a certain proximity (radius) of a player.
     *
     * @param ball   The ball.
     * @param player The player.
     * @param rayon  The radius within which the ball is considered near the player.
     * @return True if the ball is near the player, false otherwise.
     */
    public static boolean isBallNear(Ball ball, Player player, int rayon)
    {
        if (ball == null)
        {
            return false;
        }

        double distance = getDistance(player, ball);

        return distance <= rayon;
    }


    /**
     * Given the ball's current position, direction (directionX, directionY), and the field boundaries, this method calculates the ball's trajectory and updates its position.
     *
     * @param ball        The ball.
     * @param directionX  The x-component of the desired direction.
     * @param directionY  The y-component of the desired direction.
     * @param field       The soccer field.
     */
    public static void calculateTrajectory(Ball ball, int directionX, int directionY, Field field)
    {
        double vitesse = 1.0;

        int x = ball.getX();
        int y = ball.getY();

        double dx = vitesse * directionX;
        double dy = vitesse * directionY;

        int newX = (int) (x + dx);
        int newY = (int) (y + dy);

        if (newX >= 0 && newX < field.getLength() && newY >= 0 && newY < field.getHeight())
        {
            ball.setX(newX);
            ball.setY(newY);
        }
        else
        {
            ball.setX(field.getLength() / 2);
            ball.setY(field.getHeight() / 2);
        }
    }
}
