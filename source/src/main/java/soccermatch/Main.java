/**
 * Main class contains the main entry point for starting the soccer match simulation.
 */
package soccermatch;

import soccermatch.gui.SoccerSimulationGUI;

public class Main
{
    /**
     * main method to start the soccer match simulation.
     *
     * @param args Command-line arguments (not used in this application).
     */
    public static void main(String[] args)
    {
        SoccerSimulationGUI sim = new SoccerSimulationGUI();
        sim.run();
    }
}
