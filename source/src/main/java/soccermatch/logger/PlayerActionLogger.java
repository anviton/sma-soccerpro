/**
 * The PlayerActionLogger class for logging player actions in a soccer match.
 */
package soccermatch.logger;

import soccermatch.entities.Player;
import soccermatch.observer.PlayerActionListener;

/**
 * The PlayerActionLogger class logs various player actions such as taking the ball, throwing the ball,
 * marking, and stopping the ball.
 */
public class PlayerActionLogger implements PlayerActionListener {

    /**
     * Logs when a player takes possession of the ball.
     *
     * @param player The player who took the ball.
     */
    @Override
    public void onPlayerTookBall(Player player)
    {
        System.out.print("[Team " + player.getTeam().getName() + "] : " +
                player.getStatus(player.getClass()) + player.getId() + " took the ball.\n");
    }

    /**
     * Logs when a player throws the ball to another player.
     *
     * @param player The player who threw the ball.
     * @param target The player to whom the ball was thrown.
     */
    @Override
    public void onPlayerThrewBall(Player player, Player target)
    {
        System.out.print("[Team " + player.getTeam().getName() + "] : " +
                player.getStatus(player.getClass()) + player.getId() + " threw the ball to " +
                target.getStatus(target.getClass()) + target.getId() + ".\n");
    }

    /**
     * Logs when a player marks an opponent.
     *
     * @param player The player who marked an opponent.
     */
    @Override
    public void onShoot(Player player)
    {
        System.out.print("[Team " + player.getTeam().getName() + "] : " +
                player.getStatus(player.getClass()) + player.getId() + " marked !\n");
    }

    /**
     * Logs when a player stops the ball.
     *
     * @param player The player who stopped the ball.
     */
    @Override
    public void onStopBall(Player player)
    {
        System.out.print("[Team " + player.getTeam().getName() + "] : " +
                player.getStatus(player.getClass())  + player.getId() + " stop the ball.\n");
    }
}
