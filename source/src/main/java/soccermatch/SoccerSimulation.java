/**
 * SoccerSimulation class represents the main simulation for a soccer match.
 * It extends SimState from the MASON framework for agent-based modeling.
 */

package soccermatch;

import sim.util.*;
import sim.engine.*;
import sim.field.continuous.*;
import soccermatch.entities.*;
import soccermatch.generator.RandomGenerator;

/**
 * The SoccerSimulation class represents the main simulation for a soccer match.
 * It extends SimState from the MASON framework for agent-based modeling.
 */

public class SoccerSimulation extends SimState
{
    /** The width of the soccer field. */
    public static final int FIELD_WIDTH              = 40;
    /** The height of the soccer field. */
    public static final int FIELD_HEIGHT             = 40;
    /** The number of attackers per team. */
    public static final int NUMBER_ATTACKERS_PER_TEAMS = 3;
    /** The number of defenders per team. */
    public static final int NUMBER_DEFENDERS_PER_TEAMS = 2;
    /** The continuous 2D field representing the soccer field. */
    private Continuous2D yard;
    /** The match object that contains the soccer match details. */
    private Match match;


    /**
     * Constructs a new SoccerSimulation with the given random seed.
     *
     * @param seed The random seed for the simulation.
     */
    public SoccerSimulation(long seed)
    {
        super(seed);
        RandomGenerator.setInstance(super.random);

        yard  = new Continuous2D(1.0, 40, 40);

        match = new Match(new Team("A", 1),
                new Team("B", 2),
                new Field(FIELD_WIDTH, FIELD_HEIGHT), new Ball((int)
                (yard.getWidth() * 0.5), (int) (yard.getWidth() * 0.5)));

        Goal goalA = new Goal(match.getTeamA(), "Goal", 3);
        match.getTeamA().setGoal(goalA);

        Goal goalB = new Goal(match.getTeamB(), "Goal", 3);
        match.getTeamB().setGoal(goalB);

        createAttackers(match.getTeamA());
        createAttackers(match.getTeamB());

        createDefenders(match.getTeamA());
        createDefenders(match.getTeamB());
    }

    /**
     * Prepare the soccer simulation.
     */
    public void start() {
        yard.clear();

        Ball ball        = match.getBall();
        Double2D ballPos = new Double2D(FIELD_WIDTH / 2, FIELD_HEIGHT / 2);
        yard.setObjectLocation(ball, ballPos);

       if (ball.isPossessed())
       {
           ball.setPossessed(false);
           ball.getPlayer().getTeam().looseBall();
           ball.getPlayer().setHasTheBall(false);
       }

        placeGoals(match.getTeamA());
        placeGoals(match.getTeamB());

        match.setScoreTeamA(0);
        match.setScoreTeamB(0);

        resetPosition();
    }

    /**
     * Resets the position of players (attackers, defenders and goals) and the ball.
     */
    public void resetPosition()
    {
        int i = 0;

        for(Player player : match.getTeamA().getPlayers()){
            if(player instanceof Attacker)
            {
                placeAttackers((Attacker) player, i);
                player.setHasTheBall(false);
                i++;
            }
            else if (player instanceof Defender)
            {
                placeDefenders((Defender) player, i);
                player.setHasTheBall(false);
                i++;
            }

        }
        i = 0;

        for(Player player : match.getTeamB().getPlayers()){
            if(player instanceof Attacker) {
                placeAttackers((Attacker) player, i);
                player.setHasTheBall(false);
                i++;
            }
            else if (player instanceof Defender)
            {
                placeDefenders((Defender) player, i);
                player.setHasTheBall(false);
                i++;
            }

        }

        placeGoals(match.getTeamA());
        placeGoals(match.getTeamB());

        match.getBall().setX((int) (yard.getWidth()  * 0.5));
        match.getBall().setY((int) (yard.getHeight() * 0.5));
    }

    /**
     * Places attackers on the field.
     *
     * @param attacker The attacker to place.
     * @param i The index of the attacker.
     */
    private void placeAttackers(Attacker attacker, int i)
    {
        if (attacker.getTeam().getId() == 1)
        {
            attacker.setX(2 * SoccerSimulation.FIELD_WIDTH / 3);
        }
        else
        {
            attacker.setX(SoccerSimulation.FIELD_WIDTH / 3);
        }

        attacker.setY(SoccerSimulation.FIELD_HEIGHT / 2 - 5 + i * 5);

        yard.setObjectLocation(attacker, new Double2D(attacker.getX(), attacker.getY()));
    }

    /**
     * Places defenders on the field.
     *
     * @param defender The defender to place.
     * @param i The index of the defender.
     */
    private void placeDefenders(Defender defender, int i) {
        if (defender.getTeam().getId() == 1) {
            defender.setX(defender.getTeam().getGoal().getX() - 3);
            defender.setY(defender.getTeam().getGoal().getY() - (2 - 5 + i*2));
        }
        else
        {
            defender.setX(defender.getTeam().getGoal().getX() + 3);
            defender.setY(defender.getTeam().getGoal().getY() - ( 2 - 5 + i*2));
        }

        yard.setObjectLocation(defender, new Double2D(defender.getX(), defender.getY()));
    }

    /**
     * Places the goals for a team on the field.
     *
     * @param team The team for which to place the goals.
     */
    private void placeGoals(Team team)
    {
        yard.setObjectLocation(team.getGoal(), new Double2D(team.getGoal().getX(), team.getGoal().getY()));
        team.getGoal().resetGoalPosition();
        schedule.scheduleRepeating(team.getGoal());
    }

    /**
     * Creates and places attackers for a team.
     *
     * @param team The team for which to create and place attackers.
     */
    private void createAttackers(Team team)
    {
        Field field = match.getField();

        for (int i = 0; i < NUMBER_ATTACKERS_PER_TEAMS; i++) {
            Attacker attacker = new Attacker(team, "J"+ (i + 1), i);
            team.addPlayer(attacker);
            placeAttackers(attacker, i);
            field.addPlayer(attacker, attacker.getX(), attacker.getY());
            schedule.scheduleRepeating(attacker);
        }
    }

    /**
     * Creates and places defenders for a team.
     *
     * @param team The team for which to create and place defenders.
     */
    private void createDefenders(Team team) {
        Field field = match.getField();

        for (int i = 0; i < NUMBER_DEFENDERS_PER_TEAMS; i++) {
            Defender defender = new Defender(team, "D" + (i + 1), i);
            team.addPlayer(defender);
            placeDefenders(defender, i);
            field.addPlayer(defender, defender.getX(), defender.getY());
            schedule.scheduleRepeating(defender);
        }
    }

    /**
     * Gets the match object for the simulation.
     *
     * @return The match object.
     */
    public Match getMatch() {
        return match;
    }

    /**
     * Gets the yard object for the simulation.
     *
     * @return The yard object.
     */
    public Continuous2D getYard() {
        return yard;
    }
}


